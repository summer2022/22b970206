# yolo

#### 介绍
YOLO的全称是you only look once，指只需要浏览一次就可以识别出图中的物体的类别和位置，这是Redmon等人在2016年的一篇研究论文中命名的。YOLO实现了自动驾驶汽车等前沿技术中使用的实时对象检测。本项目是在openeuler22.03下通过libtorch实现，并使用ros2-foxy框架下发布话题。
![Image text](/image/yolo.png)
#### 软件架构
软件架构说明
https://pjreddie.com/darknet/yolo/

```
yolo
├── 3rdparty
│   ├── argcomplete-1.11.1
│   ├── attrs-21.2.0
│   ├── catkin_pkg-0.4.22
│   ├── coverage-5.4
│   ├── Cython-0.29.24
│   ├── distlib-0.3.3
│   ├── docutils-0.16
│   ├── empy-3.3.4
│   ├── importlib_metadata-3.8.0
│   ├── iniconfig-1.1.1
│   ├── lark-1.0.0
│   ├── more-itertools-5.0.0
│   ├── notify2-0.3.1
│   ├── numpy-1.21.2
│   ├── packaging-21.3
│   ├── pluggy-1.0.0
│   ├── py-1.11.0
│   ├── pyparsing-2.4.7
│   ├── pytest-6.2.5
│   ├── pytest-cov-3.0.0
│   ├── pytest-repeat-0.9.1
│   ├── pytest-rerunfailures-10.2
│   ├── pytest-runner-5.3.1
│   ├── python-dateutil-2.8.1
│   ├── PyYAML-5.4
│   ├── setuptools-50.0.0
│   ├── setuptools_scm-4.1.2
│   ├── six-1.15.0
│   ├── toml-0.10.2
│   ├── typing_extensions-3.7.4
│   ├── vcstools-0.1.42
│   ├── wheel-0.33.0
│   └── zipp-1.0.0
├── build_tools
│   └── colcon
└── workspace
    └── src
        ├── ament
        ├── eclipse-cyclonedds
        ├── eProsima
        ├── osrf
        ├── ros
        ├── ros2
        ├── ros-perception
        ├── ros-planning
        ├── ros-tooling
        ├── ros-tracing
        ├── ros-visualization
        ├── yolov3_ros2
        └── temp_3rdparty
            ├── eigen-3.3.7
            ├── flann-1.9.1
            ├── libtorch
            ├── opencv-3.4.16
            └── pcl-1.12
        
```
#### 安装教程

1. 下载rpm包

进入https://117.78.1.88/package/binaries/home:voladorL:branches:openEuler:22.03:LTS/yolov3/standard_x86_64下载 yolov3-1.0.0-1.oe2203.x86_64.rpm

2. 安装rpm包
```
sudo rpm -ivh yolov3-1.0.0-1.oe2203.x86_64.rpm --nodeps --force
```


#### 使用说明

安装完成以后，在/opt/ros/foxy/lib/yolov3_ros2目录下有如下输出detectnode,则表示安装成功

```
cd /opt/ros/foxy
source setup.sh
rviz2#打开rviz

```
添加组件Image和MarkerArray，将Fixed Frame修改为base_link
修改各种配置文件路径为自己系统的绝对路径
```
cd /opt/ros/foxy
source setup.sh
ros2 run yolov3_ros2 detectnode --ros-args -p parameterPath:=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/config/parameters.txt
#记得修改成自己的parameters.txt参数文件的路径
```
根据参数文件parameters.txt中的话题去添加话题
![运行显示](/image/display.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
