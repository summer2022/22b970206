
# 基于openEuler的目标检测软件移植指南

# 一、介绍

## 简要介绍
YOLO的全称是you only look once，指只需要浏览一次就可以识别出图中的物体的类别和位置，这是Redmon等人在2016年的一篇研究论文中命名的。YOLO实现了自动驾驶汽车等前沿技术中使用的实时对象检测。本项目是在openeuler22.03下通过libtorch实现，并使用ros2-foxy框架下发布话题。


## 建议版本

建议使用版本为openEuler22.03 LTS,ros2-foxy


# 二、环境要求
## 硬件要求

建议硬件配置如下所示。

| 项目 | 说明 |
| --- | --- |
| CPU | i5算力及以上X86 CPU |
| 磁盘大小 | >40G |
| 磁盘分区 | 对磁盘分区无要求 |

## 操作系统要求

操作系统要求如下所示。

| 项目 | 版本 |
| --- | --- |
| openEuler | 22.03 x86 LTS|
| Kernel | 5.10 |

说明：

```
GUI界面使用的是优麒麟配套 UKUI

```

# 三、配置编译环境

- 安装依赖库image.png


```
pip3 install catkin_pkg==0.4.22 pyparsing==2.4.7 empy==3.3.4 \
python-dateutil==2.8.1 PyYAML==5.3.1 setuptools_scm==4.1.2 six==1.15.0 \
defusedxml==0.7.1 rospkg==1.4.0 pycryptodome==3.15.0 gnupg==2.3.1 \
vcstools==0.1.42 \
-i https://pypi.tuna.tsinghua.edu.cn/simple

```

## 配置安装

```
cd /opt/ros/foxy
source /opt/ros/foxy/setup.sh
```

# 四、参数配置

## 环境配置
```
.bashrc
export STAGEPATH=/opt/ros/foxy/share/stage
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```

## lauch文件参数更新


# 五、服务测试
```
cd /opt/ros/foxy
source setup.sh
rviz2#打开rviz

```
添加组件Image和MarkerArray，将Fixed Frame修改为base_link
修改各种配置文件路径为自己系统的绝对路径
```
cd /opt/ros/foxy
source setup.sh
ros2 run yolov3_ros2 detectnode --ros-args -p parameterPath:=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/config/parameters.txt
#记得修改成自己的parameters.txt参数文件的路径
```
根据参数文件parameters.txt中的话题去添加话题

