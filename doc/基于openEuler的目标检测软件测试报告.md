
# 基于openEuler的目标检测软件测试报告

# 一、介绍

## 简要介绍
YOLO的全称是you only look once，指只需要浏览一次就可以识别出图中的物体的类别和位置，这是Redmon等人在2016年的一篇研究论文中命名的。YOLO实现了自动驾驶汽车等前沿技术中使用的实时对象检测。本项目是在openeuler22.03下通过libtorch实现，并使用ros2-foxy框架下发布话题。



# 二、测试环境
## 测试硬件


| 项目 | 说明 |
| --- | --- |
| CPU |i-10700 |
| 磁盘大小 | 80G |
| 磁盘分区 | 对磁盘分区无要求 |
|内存|16G|

## 测试操作系统环境

操作系统要求如下所示。

| 项目 | 版本 |
| --- | --- |
| openEuler | 22.03 x86 LTS|
| Kernel | 5.10 |

说明：

```
GUI界面使用的是优麒麟配套 UKUI

```

# 三、配置环境

- 安装依赖库

```
pip3 install catkin_pkg==0.4.22 pyparsing==2.4.7 empy==3.3.4 \
python-dateutil==2.8.1 PyYAML==5.3.1 setuptools_scm==4.1.2 six==1.15.0 \
defusedxml==0.7.1 rospkg==1.4.0 pycryptodome==3.15.0 gnupg==2.3.1 \
vcstools==0.1.42 \
-i https://pypi.tuna.tsinghua.edu.cn/simple

```

- 下载rpm包
```

wget https://117.78.1.88/build/home:voladorL:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/yolov3/yolov3-1.0.0-1.oe2203.x86_64.rpm

```
- 安装rpm包
```
sudo rpm -ivh yolov3-1.0.0-1.oe2203.x86_64.rpm --nodeps -force
```


## 配置安装

```
cd /opt/ros/foxy
source /opt/ros/foxy/setup.sh
```

# 四、参数配置

## 环境配置

```
sduo pluma ~/.bashrc
export STAGEPATH=/opt/ros/foxy/share/stage
export RMW_IMPLEMENTATION=rmw_fastrtps_cpp
```
参数文件配置
```
#yolov3参数配置文件
##为注释，=前后最好不要加空格
##所有文件的路径设置为绝对路径

#网络配置文件
cfgPath=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/models/yolov3.cfg
#网络权重文件
weightsPath=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/models/yolov3.weights
namePath=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/models/cocoName.txt
#rgb和depth时间戳对准图像目录，TUM RGBD数据集
imagesPath=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/data/rgbd_dataset_freiburg3_long_office_household/associate.txt
datasetPath=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/data/rgbd_dataset_freiburg3_long_office_household/

#标志位 是否从话题中读取图像文件,1是从话题读取,0是从文件路径读取
topicOrFile=0
#是否发布图像点云，1是发布，0是不发布
publishPointCloud=0
#是否发布2D检测后的图像话题
publishImageBox=1
#是否存储检测结果
saveImageBox=0
#存储路径
savePath=/home/l/image
#话题
#接收rgb话题
rgbTopic=/camera/image_raw
#接收depth话题
depthTopic=/camera/depth
#发布marker话题
markerTopic=/yolo/marker
#发布点云话题
pointCloudTopic=/yolo/point_cloud
#发布检测之后的图像话题
imageBoxTopic=/yolo/image_box
# 相机内参
# fr1
#camera.cx=318.6
#camera.cy=255.3
#camera.fx=517.3
#camera.fy=516.5

# fr2
#camera.cx=325.1
#camera.cy=249.7
#camera.fx=520.9
#camera.fy=521.0

# fr3
camera.cx=320.1
camera.cy=247.6
camera.fx=535.4
camera.fy=539.2

camera.d0=0.2312
camera.d1=-0.7849
camera.d2=-0.0033
camera.d3=-0.0001
camera.d4=0.9172

camera.scale=5000.0
```

# 五、服务测试
```
cd /opt/ros/foxy
source setup.sh
rviz2#打开rviz
```
![输入图片说明](../image/rviz2.png)
添加组件Image和MarkerArray，将Fixed Frame修改为base_link
修改各种配置文件路径为自己系统的绝对路径
```
cd /opt/ros/foxy
source setup.sh
ros2 run yolov3_ros2 detectnode --ros-args -p parameterPath:=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/config/parameters.txt
#记得修改成自己的parameters.txt参数文件的路径
```
根据参数文件parameters.txt中的话题去添加话题
![输入图片说明](../image/display.png)


