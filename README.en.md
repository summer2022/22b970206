# yolo

#### Introduction
YOLO, whose full name is you only look once, refers to the ability to identify the class and location of objects in a diagram by browsing only once, as named in a 2016 research paper by Redmon et al. YOLO implements real-time object detection used in cutting-edge technologies such as self-driving cars. This project is implemented under openeuler 22.03 via libtorch and uses the ros2-foxy framework to publish topics under.
![Image text](/image/yolo.png)
#### Software Architecture
Software architecture description
https://pjreddie.com/darknet/yolo/

```
yolo
├── 3rdparty
│ ├── argcomplete-1.11.1
│ ├── attrs-21.2.0
│ ├─ catkin_pkg-0.4.22
│ ├─ coverage5.4
│ ├── Cython-0.29.24
│ ├─ distlib-0.3.3
│ ├── docutils-0.16
│ ├── empy-3.3.4
│ ├─ importlib_metadata-3.8.0
│ ├─ iniconfig-1.1.1
│ ├── lark-1.0.0
│ ├─ more-itertools-5.0.0
│ ├── notify2-0.3.1
│ ├─ numpy-1.21.2
│ ├─ packaging-21.3
│ ├── pluggy-1.0.0
│ ├── py-1.11.0
│ ├── pyparsing-2.4.7
│ ├── pytest-6.2.5
│ ├── pytest-cov-3.0.0
│ ├── pytest-repeat-0.9.1
│ ├── pytest-rerunfailures-10.2
│ ├── pytest-runner-5.3.1
│ ├── python-dateutil-2.8.1
│ ├── PyYAML-5.4
│ ├── setuptools-50.0.0
│ ├── setuptools_scm-4.1.2
│ ├── six-1.15.0
│ ├── toml-0.10.2
│ ├─ typing_extensions-3.7.4
│ ├─ vcstools-0.1.42
│ ├── wheel-0.33.0
│ └── zipp-1.0.0
├── build_tools
│ └── colcon
└─ workspace
    └─ src
        ├─ ament
        ├── eclipse-cyclonedds
        ├── eProsima
        ├─ osrf
        ├─ ros
        ├─ ros2
        ├─ ros-perception
        ├─ ros-planning
        ├─ ros-tooling
        ├─ ros-tracing
        ├─ ros-visualization
        ├─ yolov3_ros2
        └─ temp_3rdparty
            ├─ eigen-3.3.7
            ├─ flann-1.9.1
            ├── libtorch
            ├── opencv-3.4.16
            └── pcl-1.12
```

#### installation tutorial

1. Download the rpm package

```
wget https://117.78.1.88/build/home:voladorL:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/yolov3/yolov3-1.0.0-1.oe2203.x86_ 64.rpm

```
2. Install the rpm package

```
sudo rpm -ivh yolov3-1.0.0-1.oe2203.x86_64.rpm --nodeps --force
```


#### Instructions for use

After the installation is complete, the following output detectnode will be available in /opt/ros/foxy/lib/yolov3_ros2 directory, which means the installation is successful

```
cd /opt/ros/foxy
source setup.sh
rviz2#open rviz
```

Add components Image and MarkerArray, change Fixed Frame to base_link
Modify various configuration file paths to absolute paths on your own system

```
cd /opt/ros/foxy
source setup.sh
ros2 run yolov3_ros2 detectnode --ros-args -p parameterPath:=/home/l/ros2/yolov3-ros2/yolov3-1.0.0/workspace/src/yolov3_ros2/config/ parameters.txt#Remember to change the path to your own parameters.txt parameters file
```

Add topics according to the topics in the parameters file parameters.txt
![run display](/image/display.png)

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)